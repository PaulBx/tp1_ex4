#!/usr/bin/env python
# coding : utf-8
"""
author : paul Bouyssoux

Ce script permet de tester à l'intérieur d'un package les méthodes d'une classe
contenue dans un autre package
"""

#importation de la classe via le package
from Package_Calculator.Calculator import SimpleCalculator

#Test 1
MON_TEST = SimpleCalculator(10,5)
print("Test avec pour entiers 10 et 5")
print("somme : ", MON_TEST.fsum())
print("soustractio, : ", MON_TEST.substract())
print("multiplication : ", MON_TEST.multiply())
print("division : ", MON_TEST.divide())

#Test 2, important pour la division par 0
MON_TEST2 = SimpleCalculator(10,0)
print("Test avec pour entiers 10 et 0")
print("somme : ", MON_TEST2.fsum())
print("soustractio, : ", MON_TEST2.substract())
print("multiplication : ", MON_TEST2.multiply())
print("division : ", MON_TEST2.divide())
