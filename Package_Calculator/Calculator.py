#!/usr/bin/env python
# coding : utf-8
"""
author : paul bouyssoux

-Ce script permet de créer une classe contenant les fonctions
 pour créer un calculateur simple (+,-,*,/).

"""

#Definition de la classe
class SimpleCalculator:
    """simple classe model, SimpleCalculator class"""

    def __init__(self, mon_entier_1, mon_entier_2):
        """constructeur de la classe, permet d'initialiser les attributs de la classe
           correspondant à deux entiers qui vont être utilisé dans les opérations.

        Parameters :
        mon_entier_1 (int) : premier entier

        mon_entier_2 (int) : deuxième entier

        Returns : 
        les attributs de la classe vont être mis à jour
        """
        self.mon_entier_1 = mon_entier_1
        self.mon_entier_2 = mon_entier_2

    #Définition de la méthode somme
    def fsum(self):
        """Fonction permettant d'effectuer la somme des attributs de l'objet
        de la classe

        Parameters : 
        self.mon_entier_1 (int) : premier attribut de la classe

        self.mon_entier_2 (int) : deuxième attribut de la classe

 
        returns : 
        int : somme des deux attributs de la classe        

        """
        return self.mon_entier_1 + self.mon_entier_2

    #Définition de la méthode soustraction
    def substract(self):
        """Fonction permettant d'effectuer la soustraction des attributs de l'objet
        de la classe

        Parameters : 
        self.mon_entier_1 (int) : premier attribut de la classe

        self.mon_entier_2 (int) : deuxième attribut de la classe

 
        returns : 
        int : soustraction des deux attributs de la classe        

        """
        return self.mon_entier_1 - self.mon_entier_2

    #Définition de la méthode multiplier
    def multiply(self):
        """Fonction permettant d'effectuer la multiplication des attributs de l'objet
        de la classe

        Parameters : 
        self.mon_entier_1 (int) : premier attribut de la classe

        self.mon_entier_2 (int) : deuxième attribut de la classe

 
        returns : 
        int : multiplication des deux attributs de la classe        

        """
        return self.mon_entier_1 * self.mon_entier_2

    #D&finition de la méthode diviser
    def divide(self):
        """Fonction permettant d'effectuer la division des attributs de l'objet
        de la classe

        Parameters : 
        self.mon_entier_1 (int) : premier attribut de la classe

        self.mon_entier_2 (int) : deuxième attribut de la classe

 
        returns : 
        float : division des deux attributs de la classe si le deuxième attribut de la classe est différent de 0  

        str : Phrase indiquant que la division est impossible car le deuxième attribut de la classe est nul   

        """
        if self.mon_entier_2 != 0:
            return self.mon_entier_1 / self.mon_entier_2
        return "vous avez essayé de diviser par zéro"


